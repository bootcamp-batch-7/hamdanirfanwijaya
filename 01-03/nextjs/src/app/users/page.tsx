"use client";

import { Container } from "@components";
import React, { useState } from "react";
import ListUser from "./listUser";

export const metadata = {
  title: "Users",
};

export default function Users() {
  const [search, setSearch] = useState("");

  const onSubmited = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const inputResult = event?.currentTarget[0] as HTMLInputElement;

    console.log("MENCARI", inputResult.value);
    setSearch(inputResult.value);
  };
  return (
    <Container>
      <div className="min-h-screen flex flex-1  flex-col">
        <h1 className="text-4xl font-bold">Cari Users</h1>

        <form className=" flex mt-5 rounded-xl" onSubmit={onSubmited}>
          <input
            type="text"
            className="flex-1  bg-gray-400 rounded-l-xl p-2 text-black"
          />
          <button className="bg-lime-600 rounded-r-xl px-5 py-2">
            Cari User
          </button>
        </form>

        <ListUser search={search} />
      </div>
    </Container>
  );
}
