import { MovieDBSatellite } from '@services/satellite';
import React from 'react'
import Image from 'next/image';
import { Container } from '@components';


async function getMovieData(param: string) {
    let res;
    await MovieDBSatellite.get('/movie/' + param)
        .then((response) => {
            res = response.data;
        }).catch((error) => {
            console.log("error", error);
        });
    return res;
}

export default async function getMovie({ params }: { params: { slug: string } }) {
    const data = await getMovieData(params.slug) as any
    return (
        <Container>
            <div className='min-h-screen flex flex-row'>
                <Image className='rounded-3xl' src={`https://image.tmdb.org/t/p/w500/${data?.poster_path}`} alt={`Poster ${data?.title}`} width={500} height={500}></Image>
                <div className='ms-10'>
                    <h1 className="text-lg font-bold">{data?.title}</h1>
                    <p>Release date <span className='font-extrabold bg-white text-black'>{data?.release_date}</span></p>
                    <p className='text-justify'>{data?.overview}</p>
                </div>

            </div>
        </Container>
    )
}

export const metadata = {
    title: `Gallery`
};
