"use client"
import { Container } from '@components';
import { MovieDBSatellite } from '@services/satellite';
import Link from 'next/link';
import React, { useState } from 'react'
import useSWR from 'swr';
import Image from 'next/image';

const fetcher = (url: string) => MovieDBSatellite.get(url).then(res => res.data)

export const metadata = {
  title: "Gallery"
};

export default function MovieCategories() {
  const [activeCategory, setActiveCategory] = useState('now_playing');
  const categories = [
    { id: 'now_playing', name: 'Now Playing' },
    { id: 'popular', name: 'Popular' },
    { id: 'top_rated', name: 'Top Rated' },
    { id: 'upcoming', name: 'Upcoming' },
  ];

  const { data: nowPlayingData, error: nowPlayingError, isLoading: nowPlayingLoading } = useSWR(
    'movie/now_playing',
    fetcher
  );

  const { data: popularData, error: popularError, isLoading: popularLoading } = useSWR(
    'movie/popular',
    fetcher
  );

  const { data: topRatedData, error: topRatedError, isLoading: topRatedLoading } = useSWR(
    'movie/top_rated',
    fetcher
  );

  const { data: upcomingData, error: upcomingError, isLoading: upcomingLoading } = useSWR(
    'movie/upcoming',
    fetcher
  );

  const getActiveData = () => {
    switch (activeCategory) {
      case 'now_playing':
        return nowPlayingData;
      case 'popular':
        return popularData;
      case 'top_rated':
        return topRatedData;
      case 'upcoming':
        return upcomingData;
      default:
        return null;
    }
  };

  const activeData = getActiveData();
  const isLoading = nowPlayingLoading || popularLoading || topRatedLoading || upcomingLoading;
  const totalResults = activeData?.results?.length || 0;

  const [showAll, setShowAll] = useState(false);
  const initialResults = 4;
  const resultsToShow = showAll ? totalResults : initialResults;

  return (
    <Container>
      <div className='min-h-screen'>
        <h1 className='font-bold text-2xl'>Movie Categories</h1>
        <div className="mt-4 mb-5 flex">
          {categories.map(category => (
            <button
              key={category.id}
              onClick={() => setActiveCategory(category.id)}
              className={`hover:bg-blue-500 mr-4 px-4 py-2 rounded-3xl ${activeCategory === category.id ? 'bg-blue-500 text-white' : 'bg-gray-300 text-black'}`}
            >
              {category.name}
            </button>
          ))}
        </div>
        {isLoading ? (
          <p>Loading...</p>
        ) : (
          <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 gap-4">
            {activeData?.results?.slice(0, resultsToShow)?.map((item: any, index: number) => (
              <Link key={index} href={`/gallery/${item.id}`}>
                <div className="custom-shadow bg-sky-300 px-5 py-2 rounded-md text-black">
                  <Image
                    src={`https://image.tmdb.org/t/p/w500/${item.poster_path}`}
                    alt={item.title}
                    width={500}
                    height={750}
                    className="w-full h-100 object-cover rounded-t-md"
                  />
                  <h1 className="text-sm font-bold mt-2">{item.title}</h1>
                </div>
              </Link>
            ))}
          </div>
        )}
        {totalResults > initialResults && (
          <div className="mt-4">
            {showAll ? (
              <button
                onClick={() => setShowAll(false)}
                className="bg-blue-500 text-white px-4 py-2 rounded-md"
              >
                Show Less
              </button>
            ) : (
              <button
                onClick={() => setShowAll(true)}
                className="bg-blue-500 text-white px-4 py-2 rounded-md"
              >
                Show All
              </button>
            )}
          </div>
        )}
      </div>
    </Container>
  );
}
