import axios from "axios";

const MOVIEDB_API_KEY =
  "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJkMzA2ZTY3NjQ4MGM5OGQwZTdlZWNmNmZhMGFjNWQzZSIsInN1YiI6IjY0YmM0MWEzYzhhMmQ0MDBhZmZmNDM0OSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.4DVEaYIWGCUDDeLOQgjxDQXg17crLolFg4g6U9WvbwI";

const movieDBConfig = {
  baseURL: "https://api.themoviedb.org/3",
  timeout: 10000,
  headers: {
    Authorization: `Bearer ${MOVIEDB_API_KEY}`,
  },
};

const githubConfig = {
  baseURL: "https://api.github.com",
  timeout: 10000,
};

const MovieDBSatellite = axios.create(movieDBConfig);
const GitHubSatellite = axios.create(githubConfig);

export { MovieDBSatellite, GitHubSatellite };
