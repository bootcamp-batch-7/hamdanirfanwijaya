const images = {
  LOGO_BERIJALAN: "/images/logoBerijalan.png",
  LOGO: "/images/logo.png",
  DARK: "/images/dark.png",
  LIGHT: "/images/light.png",
};

export default images;
